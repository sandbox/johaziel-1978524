<?php

/**
* Implementation of hook_views_handlers()
* /
function ekov_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ekov') . '/views',
    ),
    'handlers' => array(
      'ekov_handler_field_node_ict' => array(
        'parent' => 'views_handler_field_node',
      ),
    ),
  );
}

/**
* Implementation of hook_views_data_alter()
*/
function ekov_views_data() {
  //$data['node']['table']['group'] = t('Content');
  // Content type
  $data['node']['ict'] = array(
    'title' => t('Ict'), // The item it appears as on the UI,
    'help' => t('Icone content typa.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'ekov_handler_field_node_ict',
    ),
  );
  return $data;
}

