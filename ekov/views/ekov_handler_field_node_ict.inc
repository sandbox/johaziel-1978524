<?php

/**
 * Field handler to translate a node type into its readable form.
 *
 * @ingroup views_field_handlers
 */
class ekov_handler_field_node_ict extends views_handler_field_node {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
    $this->additional_fields['nid'] = 'nid';
  }
  /**
    * Render node type as human readable name, unless using machine_name option.
    */


  function render($values) {
    $value = $this->get_value($values);
    $nid = $this->get_value($values, 'nid');

    return 'ict '.$nid;
  }
  
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}
