<?php

	define('VID_OBJET',1);
	define('VID_ETAPE',3);
	define('VID_NIVEAU',2);
	define('VID_ACTION',7);
	define('VID_DOMAINE',8);

function ekomundi_concat($terms){
	global $language ;
	global $user;
	$langcode =  $language->language ;
	$output='';
	
  if(!is_array($terms[VID_ACTION])){
    $tid= $terms[VID_ACTION];
    $terms[VID_ACTION]=array($tid);
  }
  if(!is_array($terms[VID_DOMAINE])){
    $tid= $terms[VID_DOMAINE];
    $terms[VID_DOMAINE]=array($tid);
  }
  if(is_array($terms[VID_OBJET])){
    $tid= $terms[VID_OBJET][0];
    $terms[VID_OBJET]=$tid;
  }
  
  if(isset($terms[VID_ETAPE]) && isset($terms[VID_NIVEAU])){
    //recupere la conjugaison dans action suivant l'etape et le niveau.
    $etape=taxonomy_term_load($terms[VID_ETAPE]);
    //$niveau=taxonomy_term_load($terms[VID_NIVEAU]);

    $genre=$terms[VID_NIVEAU];
    $temps=$etape->field_txt_temps_1[$langcode]['0']['value'];
  }
  else{
    $genre=$temps='';
  }

	$format_objet='';
	$format_actions='';
  
	foreach($terms[VID_ACTION] as $tid){
		
		$action=taxonomy_term_load($tid);
    if(empty($genre) && empty($temps) ){
      $conjugaison='"'.$action->{'field_txt_term_format_2'}[$langcode]['0']['value'].'" ';
    }
    else {
      $conjugaison=$action->{'field_txt_'.$temps.'_'.$genre.'_1'}[$langcode]['0']['value'];
    }
		
	
		//recupere la formatage de l'objet
		$objet=taxonomy_term_load($terms[VID_OBJET]);
		$format_objet=$objet->field_txt_term_format_1[$langcode]['0']['value'];

		//remplace ce que suivant l'action
		if(trim(drupal_strtolower($format_objet))=='ce que'){
	 	//dpm($action);

			$field=str_replace(' ','_',trim(drupal_strtolower($format_objet)));
			//dpm($field);		

			if(!empty($action->{'field_txt_'.$field.'_1'}[$langcode]['0']['value']) ){
				$format_objet=$action->{'field_txt_'.$field.'_1'}[$langcode]['0']['value'];
			}

			$format_after=$action->{'field_txt_'.$field.'_after_1'}[$langcode]['0']['value'];

		}
		else{
			$format_after=$action->field_txt_term_format_1[$langcode]['0']['value'];

		}
		$format_actions.=trim($conjugaison.$format_after).', ';
	}
	$format_actions=drupal_substr($format_actions,0,-2).' ';


	$format_domaines='';
	$first_domaine=true;
	foreach($terms[VID_DOMAINE] as $tid){
		//recupere la formatage du domaine
		$domaine=taxonomy_term_load($tid);
		$format_domaine=$domaine->field_txt_term_format_1[$langcode]['0']['value'];
		if(!$first_domaine)
			$format_domaine=str_replace('dans le domaine',' ',$format_domaine);
	
		$format_domaines.=$format_domaine.', ';
		$first_domaine=false;
	}
	$format_domaines=drupal_substr($format_domaines,0,-2);

	$output=$format_objet.$format_actions.$format_domaines.'.';

/*
		$term=taxonomy_term_load($tid);
			//$voc_point[$vocabularies[$vid]->machine_name]=array('tid'=>$tid,'name'=>drupal_strtolower($term_det->name));
			$tname=i18n_taxonomy_term_name($term,$langcode);
			$voc_point[$vocabularies[$vid]->machine_name]=array('tid'=>$tid,'name'=>drupal_strtolower($tname));		

	*/
	
	return $output;
}

